package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.model.User;

import java.util.List;

public interface IUserService {
    boolean isLoginExists(String login);

    boolean isEmailExists(String email);

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User setPassword(String userId, String password);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);
}
