package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskCreateCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "create-task";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Create task";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
