package ru.tsc.karbainova.tm.command.task;

import ru.tsc.karbainova.tm.command.TaskAbstractCommand;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.util.TerminalUtil;

public class TaskBindTaskToProjectByIdCommand extends TaskAbstractCommand {
    @Override
    public String name() {
        return "bind-task-to-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project";
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        if (project == null) throw new TaskNotFoundException();
        final Task taskUpdate = serviceLocator.getProjectToTaskService().taskBindById(userId, projectId, taskId);
        if (taskUpdate == null) throw new TaskNotFoundException();
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }
}
